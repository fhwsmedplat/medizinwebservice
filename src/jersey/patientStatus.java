package jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
@Path("/patient")
public class patientStatus {
	@GET 
	@Path("/patientstat")  
	@Produces(MediaType.APPLICATION_JSON) 
	public String giveStatus(){
		Object[] obj = null;
		String response = "";
		obj = getPatient();
		String[] lastName = (String[]) obj[0];
		String[] firstName = (String[]) obj[1];
		String[] status = (String[]) obj[2];
	    response = Service.constructJSON(lastName, firstName, status);
		// Return a JSON Object
		return response;

	}

	// creating empty Object to fill it with data from DB
	private Object[] getPatient(){
		System.out.println("Inside checkCredentials");
		Object[] obj = null;
			try {
					obj = DBConnectionJDBC.patientenStats();
				} catch (Exception e) {
					e.printStackTrace();
				}
				

		return obj;
	}

}