package jersey;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Service {
	
	
	// Checking if the String is null
	public static boolean isNotNull(String txt) {
		return txt != null;
	}

	
	public static String constructJSON(boolean status) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("status", new Boolean(status));
		} catch (JSONException e) {
		}
		return obj.toString();
	}

	
	public static String constructJSON(boolean status,String err_msg) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("status", new Boolean(status));
			obj.put("error_msg", err_msg);
		} catch (JSONException e) {
		}
		return obj.toString();
	}
	
	public static String constructJSON(Integer[] array, String[] array2) {
		JSONObject mainObj = new JSONObject();
		try {
		int k = array.length;
		JSONArray firstArray = new JSONArray();
		for (int i = 0; i < k;i++){
		firstArray.put(array[i]);
		}
		
		int n = array2.length;
		JSONArray secondArray = new JSONArray();
		for (int i = 0; i < n;i++){
			secondArray.put(array2[i]);
			}
		

		

		
		mainObj.put("array1", firstArray);
		mainObj.put("array2", secondArray);
		} catch (JSONException e) {
		}
		return mainObj.toString();
	}
	
	public static String constructJSON(String[] array, String[] array2, String[] array3){
		JSONObject mainObj = new JSONObject();
		try{
			int k = array.length;
			JSONArray firstArray = new JSONArray();
			for (int i = 0;i < k;i++){
				firstArray.put(array[i]);
			}
			
			int n = array.length;
			JSONArray secondArray = new JSONArray();
			for (int i = 0;i < n;i++){
				secondArray.put(array2[i]);
			}
			
			int m = array.length;
			JSONArray thirdArray = new JSONArray();
			for (int i = 0;i < m;i++){
				thirdArray.put(array3[i]);
			}
			
			
			mainObj.put("array1", firstArray);
			mainObj.put("array2", secondArray);
			mainObj.put("array3", thirdArray);
		} catch (JSONException e) {
		}
		
		
		return mainObj.toString();
		
	}
	

}
