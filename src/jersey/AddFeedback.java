package jersey;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
@Path("/feedback")
public class AddFeedback {
	@GET 
	@Path("/addfeedback")  
	@Produces(MediaType.APPLICATION_JSON) 
	public String addFb(@QueryParam("feedback") String feedback, @QueryParam("rating") int rating){
		String response = "";
		int retCode = AddingFeedback(feedback, rating);
		if(retCode == 0){
			response = Service.constructJSON(true);
		}else if(retCode == 1){
			response = Service.constructJSON(false, "You already gave feedback");
		}else if(retCode == 3){
			response = Service.constructJSON(false, "Error occured");
		}
		// Return a JSON Object
		return response;

	}

	// Checking the entered parameter before DB request and adding different cases for error
	private int AddingFeedback(String feedback, int rating){
		System.out.println("Inside checkCredentials");
		int result = 3;
		if(Service.isNotNull(feedback)){
			try {
				if(DBConnectionJDBC.insertFeedback(feedback, rating)){
					System.out.println("AddFeedback if");
					result = 0;
				}
			} catch(SQLException sqle){
				System.out.println("AddFeedback catch sqle");
				if(sqle.getErrorCode() == 1062){
					result = 1;
				} 
			}
			catch (Exception e) {
				System.out.println("Inside checkCredentials catch e ");
				result = 3;
			}
		}else{
			System.out.println("Inside checkCredentials else");
			result = 3;
		}

		return result;
	}

}
