package jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
//Path: http://localhost/<application-folder-name>/login
@Path("/login")
public class Login {
	// HTTP Get Method
	@GET 
	// Path: http://localhost/<application-folder-name>/login/dologin
	@Path("/dologin")
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/login/dologin?username=abc&password=xyz
	public String doLogin(@QueryParam("username") String username, @QueryParam("password") String password){
		String response = "";
		if(checkData(username, password)){
			response = Service.constructJSON(true);
		}else{
			response = Service.constructJSON(false, "Incorrect Username or Password");
		}
	// Return a JSON Object
	return response;		
	}
	
	// Checking the entered parameter before DB request
	private boolean checkData(String username, String password){
		System.out.println("Inside checkCredentials");
		boolean result = false;
		if(Service.isNotNull(username) && Service.isNotNull(password)){
			try {
				result = DBConnectionJDBC.UserLogin(username, password);
			} catch (Exception e) {
				result = false;
			}
		}else{
			result = false;
		}

		return result;
	}

}
