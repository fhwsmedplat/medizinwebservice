package jersey;

	import javax.ws.rs.GET;
	import javax.ws.rs.Path;
	import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
	@Path("/doctor")
	public class PatientHistory {
		@GET 
		@Path("/patientHistory")  
		@Produces(MediaType.APPLICATION_JSON) 
		public String getHistory(@QueryParam("Lastname") String lastName, @QueryParam("Firstname") String firstName){
			Object[] obj = null;
			String response = "";
			obj = getHisCheck(lastName, firstName);
			String[] Date = (String[]) obj[0];
			String[] Disease = (String[]) obj[1];
			String[] Medicament = (String[]) obj[2];
		    response = Service.constructJSON(Date, Disease, Medicament);
			// Return a JSON Object
			return response;

		}

		// creating empty Object to fill it with data from DB
		private Object[] getHisCheck(String lastName,String firstName){
			System.out.println("Inside checkCredentials");
			Object[] obj = null;
				try {
						obj = DBConnectionJDBC.PatientHistory(lastName, firstName);
					} catch (Exception e) {
						e.printStackTrace();
					}
					

			return obj;
		}

	}

