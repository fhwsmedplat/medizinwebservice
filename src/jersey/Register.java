package jersey;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/register")
public class Register {
	// HTTP Get Method
	@GET 
	@Path("/doregister")  
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?username=pqrs&password=abc&email=xyz&firstName=martin&lastName=m�ller&status=Ja
	public String doRegister(@QueryParam("username") String username, @QueryParam("password") String password, @QueryParam("email") String email, @QueryParam("firstName") String firstName, @QueryParam("lastName") String lastName, @QueryParam("state") String state){
		String response = "";
		int retCode = registerUser(username, password, email, firstName, lastName, state);
		if(retCode == 0){
			response = Service.constructJSON(true);
		}else if(retCode == 1){
			response = Service.constructJSON(false, "You are already registered");
		}else if(retCode == 2){
			response = Service.constructJSON(false, "Special Characters are not allowed in Username and Password");
		}else if(retCode == 3){
			response = Service.constructJSON(false, "Error occured");
		}
		// Return a JSON Object
		return response;

	}

	
	// Checking the entered parameter before DB request and adding different cases for error
	private int registerUser(String username, String password, String email, String firstName, String lastName, String state){
		System.out.println("Inside checkCredentials");
		int result = 3;
		if(Service.isNotNull(password) && Service.isNotNull(email)){
			try {
				if(DBConnectionJDBC.insertUser(username, password, email, firstName, lastName,state)){
					System.out.println("RegisterUSer if");
					result = 0;
				}
			} catch(SQLException sqle){
				System.out.println("RegisterUSer catch sqle");
				//When Primary key violation occurs that means user is already registered
				if(sqle.getErrorCode() == 1062){
					result = 1;
				} 
				//If special characters are used in Name or password
				else if(sqle.getErrorCode() == 1064){
					System.out.println(sqle.getErrorCode());
					result = 2;
				}
			}
			catch (Exception e) {
				System.out.println("Inside checkCredentials catch e ");
				result = 3;
			}
		}else{
			System.out.println("Inside checkCredentials else");
			result = 3;
		}

		return result;
	}

}
