package jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/Drlogin")

public class ArztLogin {
		@GET 
		@Path("/doDrlogin")
		@Produces(MediaType.APPLICATION_JSON) 
		public String doDrLogin(@QueryParam("drUsername") String drUsername, @QueryParam("drPassword") String drPassword){
			String response = "";
			if(checkCredentials(drUsername, drPassword)){
				response = Service.constructJSON(true);
			}else{
				response = Service.constructJSON(false, "Incorrect Username or Password");
			}
		// Return a JSON Object
		return response;		
		}

	
		// Checking the entered parameter before DB request
		private boolean checkCredentials(String drUsername, String drPassword){
			System.out.println("Inside checkCredentials");
			boolean result = false;
			if(Service.isNotNull(drUsername) && Service.isNotNull(drPassword)){
				try {
					result = DBConnectionJDBC.DoctorLogin(drUsername, drPassword);
				} catch (Exception e) {
					result = false;
				}
			}else{
				result = false;
			}

			return result;
		}

	}